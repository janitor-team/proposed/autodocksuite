#!/usr/bin/make -f

# see http://www.gentoo.org/proj/en/qa/asneeded.xml
LDFLAGS+=-Wl,--as-needed

# link time optimisation - as soon as gcc-4.5 hits the archive
# 20.02.2013, ta, does not really work yet
#CFLAGS += -flto
CXXFLAGS += -flto
LDFLAGS += -flto

SAVEFILES=Makefile.in aclocal.m4 config.guess config.sub configure

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

%:
	dh $@

override_dh_auto_configure:
	mkdir -p debian/autodock_save
	cd autodock && cp -a $(SAVEFILES) ../debian/autodock_save && autoreconf -i
	dh_auto_configure --sourcedirectory=autodock
	mkdir -p debian/autogrid_save
	cd autogrid && cp -a $(SAVEFILES) ../debian/autogrid_save && autoreconf -i
	dh_auto_configure --sourcedirectory=autogrid

override_dh_auto_build:
	dh_auto_build --sourcedirectory=autodock
	dh_auto_build --sourcedirectory=autogrid


override_dh_auto_clean:
	dh_auto_clean --sourcedirectory=autodock
	dh_auto_clean --sourcedirectory=autogrid
	rm -rf autodock/usr autogrid/usr
	rm -rf autodock/compile autogrid/compile
	rm -rf autogrid/mingw_* autogrid/parse_param_line.* autogrid/printdate.* autogrid/printhms.* autogrid/read_parameter_library* autogrid/stop.* autogrid/timesys*
	if [ -d debian/autodock_save ] ; then mv debian/autodock_save/* autodock; rmdir debian/autodock_save ; fi
	if [ -d debian/autogrid_save ] ; then mv debian/autogrid_save/* autogrid; rmdir debian/autogrid_save ; fi
	# remove test results
	rm -rf autodock/Tests/popfile autodock/Tests/test_1pgp*.dlg
	rm -rf autodock_testresults
	rm -rf autogrid/Tests/hsg1_sm.*.map autogrid/Tests/hsg1_sm.glg autogrid/Tests/hsg1_sm.maps* autogrid/Tests/test_x1* autogrid/Tests/x1hpv.*.map autogrid/Tests/x1hpv.maps.*
	rm -rf autogrid_testresults
	find . -name "*.pyc" -delete

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --sourcedirectory=autodock
	dh_auto_test --sourcedirectory=autogrid
endif

override_dh_install-indep:
	dh_install -i
	rm -rf $(CURDIR)/debian/autodock-test/usr/share/autodock/Tests/AutoDockTools
	rm -rf $(CURDIR)/debian/autogrid-test/usr/share/autogrid/Tests/CVS/
	rm -rf $(CURDIR)/debian/autodock-test/usr/share/autodock/Tests/CVS/

override_dh_fixperms-indep:
	dh_fixperms
	chmod 644 $(CURDIR)/debian/autodock-test/usr/share/autodock/Tests/*
	chmod 644 $(CURDIR)/debian/autogrid-test/usr/share/autogrid/Tests/*

override_dh_auto_install-arch:
	dh_auto_install --sourcedirectory=autodock
	dh_auto_install --sourcedirectory=autogrid

override_dh_installchangelogs:
	dh_installchangelogs autodock/RELEASENOTES

override_dh_missing:
	find debian -name autodock4.omp -delete
	dh_missing
